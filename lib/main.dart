import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        //Check for errors
        if (snapshot.hasError) {
          print("Something went wrong");
        }
        //once completed, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              primarySwatch: Colors.blue,
            ),
            home: const MyHomePage(title: 'Flutter Demo Home Page'),
          );
        }
        return const CircularProgressIndicator();
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var fecha = "";
  //Datos cliente
  var nombreC = "";
  var apellidoC = "";
  var observacionesC = "";
  //Datos mesero
  var nombreM = "";
  var apellido1M = "";
  var apellido2M = "";
  //Datos mesa
  var numeroComensales = "";
  var ubicacion = "";
  //Datos platillo
  var nombreP = "";
  var importeP = "";
  //Datos bebida
  var nombreB = "";
  var importeB = "";

  //Controladores textfields
  final controllerFecha = TextEditingController();
  final controllerNombreC = TextEditingController();
  final controllerApellidoC = TextEditingController();
  final controllerObservacionesC = TextEditingController();
  final controllerNombreM = TextEditingController();
  final controllerApellido1M = TextEditingController();
  final controllerApellido2M = TextEditingController();
  final controllerNumeroComensales = TextEditingController();
  final controllerUbicacion = TextEditingController();
  final controllerNombreP = TextEditingController();
  final controllerImporteP = TextEditingController();
  final controllerNombreB = TextEditingController();
  final controllerImporteB = TextEditingController();

  @override
  void dispose() {
    // TODO: implement dispose
    controllerFecha.dispose();
    controllerNombreC.dispose();
    controllerApellidoC.dispose();
    controllerObservacionesC.dispose();
    controllerNombreM.dispose();
    controllerApellido1M.dispose();
    controllerApellido2M.dispose();
    controllerNumeroComensales.dispose();
    controllerUbicacion.dispose();
    controllerNombreP.dispose();
    controllerImporteP.dispose();
    controllerNombreB.dispose();
    controllerImporteB.dispose();
    super.dispose();
  }

  void clearText() {
    controllerFecha.clear();
    controllerNombreC.clear();
    controllerApellidoC.clear();
    controllerObservacionesC.clear();
    controllerNombreM.clear();
    controllerApellido1M.clear();
    controllerApellido2M.clear();
    controllerNumeroComensales.clear();
    controllerUbicacion.clear();
    controllerNombreP.clear();
    controllerImporteP.clear();
    controllerNombreB.clear();
    controllerImporteB.clear();
  }

  //Adding student
  CollectionReference facturas =
      FirebaseFirestore.instance.collection('facturas');
  Future<void> AgregarFactura() {
    //print("User Added");
    return facturas
        .add({
          'fecha': fecha,
          'Cliente.Nombre': nombreC,
          'Cliente.Apellido': apellidoC,
          "Mesero.Nombre": nombreM,
          "Mesero.Apellido1": apellido1M,
          "Mesero.Apellido2": apellido2M,
          "Mesa.num_Comensales": numeroComensales,
          "Mesa.Ubicacion": ubicacion,
          "Platillo.Nombre": nombreP,
          "Platillo.Importe": importeP,
          "Bebida.Nombre": nombreB,
          "Bebida.Importe": importeB
        })
        .then((value) => print('factura agregada'))
        .catchError((error) => print('fallo al agregar factura: $error'));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(title: Text('Examen flutter')),
        body: Container(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerFecha,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese una fecha'),
                      ),
                    ),
                    Container(
                        margin: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          controller: controllerNombreC,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Ingrese un nombre cliente'),
                        )),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerApellidoC,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese un apellido cliente'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerObservacionesC,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese observaciones'),
                      ),
                    ),
                    Container(
                        margin: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          controller: controllerNombreM,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Ingrese un nombre mesero'),
                        )),
                    Container(
                        margin: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          controller: controllerApellido1M,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Ingrese primer apellido mesero'),
                        )),
                    Container(
                        margin: const EdgeInsets.symmetric(vertical: 10.0),
                        child: TextFormField(
                          controller: controllerApellido2M,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Ingrese segundo apellido mesero'),
                        )),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerNumeroComensales,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese cantidad de comensales'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerUbicacion,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese ubicacion de la mesa'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerNombreP,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese nombre del platillo'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerImporteP,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese importe platillo'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerNombreB,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese nombre de la bebida'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      child: TextFormField(
                        controller: controllerImporteB,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Ingrese importe bebida'),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () {
                          setState(() {
                            fecha = controllerFecha.text;
                            //Datos cliente
                            nombreC = controllerNombreC.text;
                            apellidoC = controllerApellidoC.text;
                            observacionesC = controllerObservacionesC.text;
                            //Datos mesero
                            nombreM = controllerNombreM.text;
                            apellido1M = controllerApellido1M.text;
                            apellido2M = controllerApellido2M.text;
                            //Datos mesa
                            numeroComensales = controllerNumeroComensales.text;
                            ubicacion = controllerUbicacion.text;
                            //Datos platillo
                            nombreP = controllerNombreP.text;
                            importeP = controllerImporteP.text;
                            //Datos bebida
                            nombreB = controllerNombreB.text;
                            importeB = controllerImporteB.text;
                            AgregarFactura();
                          });
                        },
                        child: Text('Ingresar factura'))
                  ],
                ))));
  }
}
